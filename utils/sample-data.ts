import { User } from "../interfaces";

/** Dummy user data. */
export const sampleUserData: User[] = [
  { id: 101, name: "Alice" },
  { id: 102, name: "Bob" },
  { id: 103, name: "Caroline" },
  { id: 104, name: "Dave" },
  { id: 105, name: "Robert" },
  { id: 106, name: "Bill" },
  { id: 107, name: "Maria" },
  { id: 108, name: "John" },
];
