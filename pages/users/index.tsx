import { GetStaticProps } from "next";
import { NextSeo } from "next-seo";
import Link from "next/link";
import Layout from "../../components/Layout";
import List from "../../components/List";
import { User } from "../../interfaces";
import { sampleUserData } from "../../utils/sample-data";

type Props = {
  items: User[];
};

const WithStaticProps = ({ items }: Props) => (
  <Layout>
    <NextSeo
      title="Listado de usuarios"
      description="En esta página podrás encontrar fantásticas personas que te aportarán valor para que puedas lograr todo lo que te propongas y nada te frene para conseguir tus sueños. Haz click en el enlace de aquí abajo para acceder al webinar gratuito!!"
    />
    <h1>Users List</h1>
    <p>
      Example fetching data from inside <code>getStaticProps()</code>.
    </p>
    <p>You are currently on: /users</p>
    <List items={items} />
    <p>
      <Link href="/">
        <a>Go home</a>
      </Link>
    </p>
  </Layout>
);

export const getStaticProps: GetStaticProps = async () => {
  // Example for including static props in a Next.js function component page.
  // Don't forget to include the respective types for any props passed into
  // the component.
  const items: User[] = sampleUserData;
  return { props: { items } };
};

export default WithStaticProps;
